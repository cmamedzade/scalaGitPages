
/**
 * this wrapper object is created for string color
 */
object Colors {

   protected object Color extends Enumeration {
    type Color = Value
    val RED, BLACK, WHITE, BLUE, YELLOW, GREEN = Value
  }
   import Color._


   /**
    * @param input string color
    * @return color object
    */
   def getColor(input: String): Color =
    input match
      case "red" => RED
      case "black" => BLACK
      case "white" => WHITE
      case "blue" => BLUE
      case "yellow" => YELLOW
      case "green" => GREEN
      case _ => throw new MatchError("no color found")


   /**
    * @param color accept only instance of color or subtype of color
    * This function is for displaying color object
    */
   def displayColor(color: Color): Unit =
    println(s"color is: ${color.toString}")
}